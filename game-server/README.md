Game Server
===========

Generates maps for the [cobot] game.

Setup
-----

To setup a virtual Python environment and install the required libraries:
```
python3 -m venv .
source ./bin/activate
pip install -r requirements.txt
```

Development
-----------

To start the development server for testing:
```
source ./bin/activate # only needs to be run once per shell session
flask run
```
