from flask import Flask, jsonify, request
from generate import generate

app = Flask(__name__)

DEFAULT_ROWS = 16
DEFAULT_COLS = 9

@app.route("/game-server")
def game_server():

    """
    http://localhost:5000/game-server?rows=2&cols=2
"nodes": [
    {
      "children": [
        {
          "distance": 4,
          "id": 1,
          "loc": [
            0,
            4
          ],
          "traffic": 3
        },
      ],
      "id": 0,
      "loc": [
        0,
        0
      ]
    }
]

"fares": [
    {
      "dropoff": [
        4,
        1
      ],
      "pickup": [
        2,
        4
      ]
    }
]
    """
    rows = request.args.get('rows', DEFAULT_ROWS, type=int)
    cols = request.args.get('cols', DEFAULT_COLS, type=int)
    l, n, f = generate(rows, cols)
    return jsonify(
        layout=l,
        nodes=n,
        fares=f
    )


