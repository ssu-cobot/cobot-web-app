import random
import math

class Node:

    def __init__(self, r, c):
        self.type = "x"
        self.r = r
        self.c = c
        self.dir = -1
        self.rot = -1
        self.children = []
        self.id = -1

    def loc(self):
        return [self.r, self.c]

    def __str__(self):
        return "[" + str(self.r) + "," + str(self.c) + "]"


def placeTurns(board):

    nodeList = []
    nodeCount = math.ceil((len(board)/2))
    i = 0
    j = 0


    while nodeCount > 0:
    
        # Randomly get a spot to place a turn
        # Place turns on every other Y line
        # Make sure subsequently placed turns aren't on adjacent or the same X columns
        # This makes sure the end result is mappable visually (there needs to be gaps between streets)
        j = random.randint(0, len(board[0]) - 1)

        if len(nodeList) != 0:
            while -1 <= (j - nodeList[int(i/2)-1].c) <= 1:
                j = random.randint(0, len(board[0]) - 1)

        # Place the turn and add it to a list of nodes
        board[i][j].type = "O"
        nodeList.append(board[i][j])
        i += 2
        nodeCount -= 1

    return nodeList

def connectTurns(nodeList, board):

    for n in range(len(nodeList)-1):
        #printGame(board)
        if not nodeList[n+1]:
            return

        r = nodeList[n+1].r - nodeList[n].r
        c = nodeList[n+1].c - nodeList[n].c

        penR = nodeList[n].r
        penC = nodeList[n].c

        # Draw along Manhattan Distance C to R
        while c != 0:
            if c < 0:
                penC -= 1
                if board[penR][penC].type != "O":
                    board[penR][penC].type = "-"
                c += 1
            if c > 0:
                penC += 1
                if board[penR][penC].type != "O":
                    board[penR][penC].type = "-"
                c -= 1

        # This is always a turn
        board[penR][penC].type = "O"

        while r != 0:
            penR += 1
            if board[penR][penC].type != "O":
                board[penR][penC].type = "|"
            r -= 1


        # Reset r, c, and pens
        r = nodeList[n + 1].r - nodeList[n].r
        c = nodeList[n + 1].c - nodeList[n].c

        penR = nodeList[n].r
        penC = nodeList[n].c

        # Draw along Manhattan Distance R to C

        while r != 0:
            penR += 1
            if board[penR][penC].type != "O":
                board[penR][penC].type = "|"
            r -= 1

        # This is always a turn
        board[penR][penC].type = "O"

        while c != 0:
            if c < 0:
                penC -= 1
                if board[penR][penC].type != "O":
                    board[penR][penC].type = "-"
                c += 1
            if c > 0:
                penC += 1
                if board[penR][penC].type != "O":
                    board[penR][penC].type = "-"
                c -= 1





def orientTurns(nodeList, board):

    for i in nodeList:
        i.type = getType(board, i)

def getType(board, n):

    # Check number of roads in each direction
    up = 0
    right = 0
    down = 0
    left = 0

    if n.c != 0:
        if board[n.r][n.c - 1].type != "x":
            left += 1
    if n.c != len(board[0])-1:
        if board[n.r][n.c + 1].type != "x":
            right += 1
    if n.r != 0:
        if board[n.r - 1][n.c].type != "x":
            up += 1
    if n.r != len(board)-1:
        if board[n.r + 1][n.c].type != "x":
            down += 1

    # A type
    if up + down + right + left == 2:
        if up == 1 and right == 1:
            return "A1"
        if right == 1 and down == 1:
            return "A2"
        if down == 1 and left == 1:
            return "A3"
        if left == 1 and up == 1:
            return "A4"
    # B type
    if up + down + right + left == 3:
        if left == 1 and up == 1 and right == 1:
            return "B1"
        if up == 1 and right == 1 and down == 1:
            return "B2"
        if right == 1 and down == 1 and left == 1:
            return "B3"
        if down == 1 and left == 1 and up == 1:
            return "B4"
    # C type
    else:
        return "C"


def stringify(board):
    string = ""
    for i in range(len(board)):
        for j in range(len(board[i])):
            type = board[i][j].type
            if type != 'x' and type != '-' and type != '|':
                string += "O"
            else:
                string += type
        if i != len(board)-1:
            string += "\\n"

    return string

def buildNodeList(board):

    nodeList = []
    count = 0
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j].type == "O":
                board[i][j].id = count
                nodeList.append(board[i][j])
                count += 1

    return nodeList

def findChild(n, dir, board):

    penR = n.r
    penC = n.c

    # Child will always be at least 1 away
    count = 1
    if dir == "right":
        penC += 1
        while board[penR][penC].id == -1:
            penC += 1
            count += 1
    elif dir == 'left':
        penC -= 1
        while board[penR][penC].id == -1:
            penC -= 1
            count += 1
    elif dir == 'up':
        penR -= 1
        while board[penR][penC].id == -1:
            penR -= 1
            count += 1
    elif dir == 'down':
        penR += 1
        while board[penR][penC].id == -1:
            penR += 1
            count += 1

    # for now we'll just use an entirely random value for traffic
    # in reality, this doesn't make sense, so a new method will be made later
    traffic = random.randint(1, 3)

    n.children.append([board[penR][penC], count, traffic])


def linkNodes(nodeList, board):

    for n in nodeList:

        if n.type != "A2" and n.type != "A3" and n.type != "B3":
            findChild(n, "up", board)
        if n.type != "A3" and n.type != "A4" and n.type != "B4":
            findChild(n, "right", board)
        if n.type != "A1" and n.type != "A4" and n.type != "B1":
            findChild(n, "down", board)
        if n.type != "A1" and n.type != "A2" and n.type != "B2":
            findChild(n, "left", board)

def printGame(board):

    for i in range(len(board)):
        print("", end='', sep='')
        for j in range(len(board[0])):
            print(board[i][j].type, end=' ', sep='')
        print("")

    print("__________________________________")



def generate(r, c):

    board = []
    #print("Rows = ", r, "Columns = ", c)
    for i in range (r):
        row = []
        for j in range(c):
            row.append(Node(i, j))
        board.append(row)

    nodeList = placeTurns(board)
    connectTurns(nodeList, board)
    #printGame(board)

    # We have to remake nodeList, because we added more turns during the connectTurns function
    nodeList = buildNodeList(board)
    orientTurns(nodeList, board)

    #printGame(board)
    linkNodes(nodeList, board)

    #print(stringify(board))
    nodes =[]
    for n in nodeList:
        nodes.append({'id': n.id, 'loc': n.loc(), 'children': jsonChildren(n)})

    fares = generateFares(nodeList, board)

    return stringify(board), nodes, fares

def jsonChildren(n):
    children = []
    for c in n.children:
        children.append({'id': c[0].id, 'loc': c[0].loc(), 'distance': c[1], 'traffic': c[2]})
    return children

def generateFares(nodeList, board):

    limit = len(nodeList)-1
    fares = []
    min = int(math.floor(limit / 2))

    for i in range(min):
        r1 = 0
        r2 = 0
        while r1 == r2:
            r1 = random.randint(0, limit)
            r2 = random.randint(0, limit)

        fares.append({'pickup': nodeList[r1].loc(), 'dropoff': nodeList[r2].loc()})

    return fares


