module Test.Main where

import Prelude

import Data.Either (Either(..))
import Effect (Effect)
import Effect.Console (logShow)
import Test.Assert (assert')
import Text.Parsing.Parser (Parser, runParser)

import Cobot.Game (Tile(..), RoadStyle(..), GroundStyle(..))
import Cobot.Game.Parser (ground, road, tile, layout)


type TestM = Effect Unit

parseTest :: forall s a. Show a => Eq a => s -> a -> Parser s a -> TestM
parseTest input expected p = case runParser input p of
  Right actual -> do
    assert' ("expected: " <> show expected <> ", actual: " <> show actual) (expected == actual)
    logShow actual
  Left err -> assert' ("error: " <> show err) false

testShowTile :: TestM
testShowTile = logShow $ Road Unmarked 0

testGround :: TestM
testGround = do
  let x = Ground Grass 0
  parseTest "x" x ground

testRoad :: TestM
testRoad = do
  parseTest "|" (Road Unmarked 0) road
  parseTest "-" (Road Unmarked 0) road
  parseTest "O" (Road Unmarked 0) road

testTile :: TestM
testTile = do
  parseTest "|" (Road Unmarked 0) tile
  parseTest "x" (Ground Grass 0) tile

testLayout :: TestM
testLayout = do
  let x = Ground Grass 0
  let o = Road Unmarked 0
  parseTest "xOx\\nOxO\\nxOx" [[x,o,x],[o,x,o],[x,o,x]] layout

main :: Effect Unit
main = do
  testShowTile
  testGround
  testRoad
  testTile
  testLayout
