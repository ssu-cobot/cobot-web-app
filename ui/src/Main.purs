module Main where

import Prelude
import Effect (Effect)
import Cherry (mount)
import Cherry.Renderer (createRenderer)
import Cobot.Core (route, store, view)


main :: Effect Unit
main = do
  renderer <- createRenderer "#main" view
  mount store renderer [ route ]
