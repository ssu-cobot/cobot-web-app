module Cobot.Game (
  Game, GameBoard, Tilesheet, Layer, Layers, Input, Point, Piece, Player, Position,
  Direction(..),
  CarStyle(..),
  GroundStyle(..),
  RoadStyle(..),
  PathStyle(..),
  Tile(..),
  update, initialGame, toPosition, toPoint
  ) where

import Prelude
import Data.Array (length, (!!))
import Data.Int (round, toNumber)
import Data.Maybe (Maybe(..))
import Graphics.Canvas (CanvasImageSource, Context2D)


type GameBoard = Array (Array Tile)

type Tilesheet = CanvasImageSource

type Layer = Context2D

type Layers = { ui :: Layer, game :: Layer, bg :: Layer }

type Point = { x :: Int, y :: Int }

toPosition :: Point -> Position
toPosition { x, y } = { x: toNumber x, y: toNumber y }

type Position = { x :: Number, y:: Number }

toPoint :: Position -> Point
toPoint { x, y } = { x: round x, y: round y }

type Player = { position :: Position, direction :: Direction }

initialPlayer :: Point -> Player
initialPlayer p = { position: toPosition p, direction: North }

type Game =
  { board     :: GameBoard
  , tilesheet :: Tilesheet
  , layers    :: Layers
  , scale     :: Number
  , player    :: Player
  }

initialGame :: GameBoard -> Tilesheet -> Layers -> Number -> Game
initialGame b = { board: b, tilesheet: _, layers: _, scale: _, player: initialPlayer p }
  where
    p = { x, y }
    y = length b / 2
    x = case b !! 0 of
      Just r -> length r / 2
      Nothing -> 0


type Piece = Int


data CarStyle
  = GreenCar
  | GreyCar
  | RedCar

derive instance eqCarStyle :: Eq CarStyle

instance showCarStyle :: Show CarStyle where
  show GreenCar = "Green"
  show GreyCar = "Grey"
  show RedCar = "Red"


data GroundStyle
  = Grass
  | Dirt
  | Stone
  | Sand

derive instance eqGroundStyle :: Eq GroundStyle

instance showGroundStyle :: Show GroundStyle where
  show Grass = "Grass"
  show Dirt = "Dirt"
  show Stone = "Stone"
  show Sand = "Sand"


data RoadStyle
  = WhiteStripe
  | YellowStripe
  | EdgeStripe
  | Crosswalk
  | ArrowMark
  | SymbolMark
  | Unmarked

derive instance eqRoadStyle :: Eq RoadStyle

instance showRoadStyle :: Show RoadStyle where
  show WhiteStripe = "White Stripe"
  show YellowStripe = "Yellow Stripe"
  show EdgeStripe = "Edge Stripe"
  show Crosswalk = "Crosswalk"
  show ArrowMark = "Arrow Mark"
  show SymbolMark = "Symbol Mark"
  show Unmarked = "Unmarked"


data PathStyle
  = DarkGreyBrick
  | SandBrick
  | LightGreyBrick

derive instance eqPathStyle :: Eq PathStyle

instance showPathStyle :: Show PathStyle where
  show DarkGreyBrick = "Dark Grey"
  show SandBrick = "Sand"
  show LightGreyBrick = "Light Grey"


data Direction
  = North
  | South
  | East
  | West

derive instance eqDirection :: Eq Direction

instance showDirection :: Show Direction where
  show North = "North"
  show South = "South"
  show East ="East"
  show West = "West"


data Tile
  = Car Direction CarStyle Piece
  | Road RoadStyle Piece
  | Ground GroundStyle Piece
  | Path PathStyle Piece

derive instance eqTile :: Eq Tile

instance showTile :: Show Tile where
  show (Car d s p) = "<Car (" <> show d <> ") {" <> show s <> "} #" <> show p <> ">"
  show (Road s p) = "<Road {" <> show s <> "} #" <> show p <> ">"
  show (Ground s p) = "<Ground {" <> show s <> "} #" <> show p <> ">"
  show (Path s p) = "<Path {" <> show s <> "} #" <> show p <> ">"


type Input =
  { left  :: Boolean
  , up    :: Boolean
  , right :: Boolean
  , down  :: Boolean
  }

update :: Input -> Game -> Game
update { left, up, right, down } game = game { player = player }
  where
    player = { position, direction }
    position = { x, y }
    x = game.player.position.x + case { left, right } of
      { left: true, right: false } -> -dx
      { left: false, right: true } ->  dx
      otherwise -> 0.0
    y = game.player.position.y + case { up, down } of
      { up: true, down: false } -> -dy
      { up: false, down: true } ->  dy
      otherwise -> 0.0
    dx = 0.125 * game.scale
    dy = 0.125 * game.scale
    direction = case { left, up, right, down } of
      { left: true, up: false, right: false, down: false } -> West
      { left: false, up: true, right: false, down: false } -> North
      { left: false, up: false, right: true, down: false } -> East
      { left: false, up: false, right: false, down: true } -> South
      otherwise -> game.player.direction
