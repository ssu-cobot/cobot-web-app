module Cobot.View.PlayerSelect (playerSelect) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, link)


playerSelect :: Content
playerSelect =
  [ driverLink
  , dispatcherLink
  ]

driverLink :: VNode
driverLink = link "player-select-driver-link" "driver" "game"

dispatcherLink :: VNode
dispatcherLink = link "player-select-dispatcher-link" "dispatcher" "game"
