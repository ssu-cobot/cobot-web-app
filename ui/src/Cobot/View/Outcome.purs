module Cobot.View.Outcome (outcome) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, widget, link)


outcome :: Content
outcome =
  [ outcomeWidget
  , playAgainLink
  , homeLink
  ]

outcomeWidget :: VNode
outcomeWidget =  widget "outcome-widget" "outcome"

playAgainLink :: VNode
playAgainLink = link "outcome-play-again-link" "play again" "game"

homeLink :: VNode
homeLink = link "outcome-home-link" "home" "landing"
