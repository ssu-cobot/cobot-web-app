module Cobot.View.Landing (landing) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, widget, link)


landing :: Content
landing =
  [ leaderboard
  , playLink
  , rulesLink
  , aboutLink
  ]

leaderboard :: VNode
leaderboard = widget "landing-leaderboard-widget" "leaderboard"

playLink :: VNode
playLink = link "landing-play-link" "play" "game"

rulesLink :: VNode
rulesLink = link "landing-rules-link" "rules" "rules"

aboutLink :: VNode
aboutLink = link "landing-about-link" "about" "about"
