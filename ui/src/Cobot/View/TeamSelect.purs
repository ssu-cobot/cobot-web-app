module Cobot.View.TeamSelect (teamSelect) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, link)


teamSelect :: Content
teamSelect =
  [ cobotLink
  , humanLink
  ]

cobotLink :: VNode
cobotLink = link "team-select-cobot-link" "cobot" "player-select"

humanLink :: VNode
humanLink = link "team-select-human-link" "human" "partner-select"
