module Cobot.View.About (about) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, link, widget)


about :: Content
about =
  [ aboutText
  , backLink
  ]

aboutText :: VNode
aboutText = widget "about-widget" "about"

backLink :: VNode
backLink = link "about-back-link" "back" "landing"
