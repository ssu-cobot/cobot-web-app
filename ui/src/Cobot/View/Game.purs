module Cobot.View.Game (game) where

import Cherry.VDOM (VNode, h, (:=))
import Cobot.View (Content, link)


game :: Content
game =
  [ uiCanvas
  , gameCanvas
  , backgroundCanvas
  , quitLink
  ]

quitLink :: VNode
quitLink = link "game-quit-link" "quit" "outcome"

canvas :: String -> VNode
canvas id = h "canvas" [ "id" := id ] []

uiCanvas :: VNode
uiCanvas = canvas "ui-layer"

gameCanvas :: VNode
gameCanvas = canvas "game-layer"

backgroundCanvas :: VNode
backgroundCanvas = canvas "background-layer"
