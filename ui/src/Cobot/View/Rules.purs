module Cobot.View.Rules (rules) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, link, widget)


rules :: Content
rules =
  [ rulesText
  , backLink
  ]

rulesText :: VNode
rulesText = widget "rules-widget" "rules"

backLink :: VNode
backLink = link "rules-back-link" "back" "landing"
