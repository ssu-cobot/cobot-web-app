module Cobot.View.JoinFriend (joinFriend) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, widget, link, field)


joinFriend :: Content
joinFriend =
  [ friendCodeWidget
  , friendCodeField
  , startLink
  ]

friendCodeWidget :: VNode
friendCodeWidget = widget "join-friend-friend-code-widget" "your friend code"

friendCodeField :: VNode
friendCodeField = field "join-friend-friend-code-field" "enter a friend's code"

startLink :: VNode
startLink = link "join-friend-start-link" "start" "player-select" -- XXX: "start"?
