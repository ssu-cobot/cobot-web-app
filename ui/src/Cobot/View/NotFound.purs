module Cobot.View.NotFound (notFound) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, widget)


notFound :: Content
notFound =
  [ notFoundWidget
  ]

notFoundWidget :: VNode
notFoundWidget = widget "not-found-widget" "page not found"
