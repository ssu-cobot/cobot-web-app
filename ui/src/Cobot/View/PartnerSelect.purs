module Cobot.View.PartnerSelect (partnerSelect) where

import Cherry.VDOM (VNode)
import Cobot.View (Content, link)


partnerSelect :: Content
partnerSelect =
  [ friendLink
  , randomLink
  ]

friendLink :: VNode
friendLink = link "partner-select-friend-link" "join a friend" "join-friend"

randomLink :: VNode
randomLink = link "partner-select-random-link" "random partner" "player-select"
