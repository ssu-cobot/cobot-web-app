module Cobot.Core
       ( Route
       , State
       , route
       , store
       , view
       ) where

import Prelude

import Cherry.Router (router)
import Cherry.Router.Parser (match, lit, end)
import Cherry.Store as S
import Cherry.VDOM (VNode)
import Cobot.Controller.Game as Game
import Cobot.View (view) as View
import Cobot.View.Game (game) as View
import Cobot.View.JoinFriend (joinFriend) as View
import Cobot.View.Landing (landing) as View
import Cobot.View.NotFound (notFound) as View
import Cobot.View.Outcome (outcome) as View
import Cobot.View.PartnerSelect (partnerSelect) as View
import Cobot.View.PlayerSelect (playerSelect) as View
import Cobot.View.TeamSelect (teamSelect) as View
import Cobot.View.Rules (rules) as View
import Cobot.View.About (about) as View
import Control.Alt ((<|>))
import Data.Maybe (Maybe(..), fromMaybe, isNothing)
import Data.String (Pattern(..), stripPrefix)
import Effect (Effect)
import Effect.Console (log)


-- MODEL

type State =
  { route :: Route
  , game :: Game.State
  }

initialState :: State
initialState =
  { route: LandingRoute
  , game: Game.initialState
  }

store :: S.Store State
store = S.createStore initialState

select :: ∀ a. (State -> a) -> Effect a
select = S.select store

reduce :: (State -> State) -> Effect Unit
reduce = S.reduce store


-- VIEW

view :: State -> VNode
view state =
  View.view case state.route of
    LandingRoute -> View.landing
    RulesRoute -> View.rules
    AboutRoute -> View.about
    TeamSelectRoute -> View.teamSelect
    PartnerSelectRoute -> View.partnerSelect
    JoinFriendRoute -> View.joinFriend
    PlayerSelectRoute -> View.playerSelect
    GameRoute -> View.game
    OutcomeRoute -> View.outcome
    NotFoundRoute -> View.notFound


-- CONTROLLER

--[ Route controller

data Route
  = LandingRoute
  | RulesRoute
  | AboutRoute
  | TeamSelectRoute
  | PartnerSelectRoute
  | JoinFriendRoute
  | PlayerSelectRoute
  | GameRoute
  | OutcomeRoute
  | NotFoundRoute

detectRoute :: String -> Route
detectRoute url = fromMaybe NotFoundRoute $ match path $
  -- /
  LandingRoute <$ end
  <|>
  -- /landing
  LandingRoute <$ (lit "landing") <* end
  <|>
  -- /rules
  RulesRoute <$ (lit "rules") <* end
  <|>
  -- /about
  AboutRoute <$ (lit "about") <* end
  <|>
  -- /team-select
  TeamSelectRoute <$ (lit "team-select") <* end
  <|>
  -- /partner-select
  PartnerSelectRoute <$ (lit "partner-select") <* end
  <|>
  -- /join-friend
  JoinFriendRoute <$ (lit "join-friend") <* end
  <|>
  -- /player-select
  PlayerSelectRoute <$ (lit "player-select") <* end
  <|>
  -- /game
  GameRoute <$ (lit "game") <* end
  <|>
  -- /outcome
  OutcomeRoute <$ (lit "outcome") <* end

  where
    path = fromMaybe url $ stripPrefix (Pattern "/dist") url

route :: Effect Unit
route = router route'
  where
    route' url = do
      let r = detectRoute url
      reduce (\s -> s { route = r })
      loadUnloadGame r

    loadUnloadGame GameRoute = do
      game <- select _.game
      when (isNothing game) $ Game.load callback
    loadUnloadGame _ = reduce (\s -> s { game = Nothing })

    callback Nothing = log "load failed"
    callback (Just game') = do
      reduce (\s -> s { game = Just game' })
      Game.start game'

--] (Route controller)
