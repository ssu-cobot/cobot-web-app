module Cobot.View (Content, link, view, widget, field) where

import Prelude
import Cherry.Router (navigateTo)
import Cherry.VDOM (VNode, h, t, (:=), (~>))


type Content = Array VNode

link :: String -> String -> String -> VNode
link id label target =
  h "a" [ "id" := id
        , "onClick" ~> (const $ navigateTo target)
        ]
    [ h "span" [] [ t $ "[ " <> label <> " ]" ]
    ]

view :: Content -> VNode
view = h "view" []

widget :: String -> String -> VNode
widget id label =
  h "widget" [ "id" := id ]
    [ h "span" [] [ t $ "< " <> label <> " >" ]
    ]

field :: String -> String -> VNode
field id label =
  h "input" [ "id" := id
            , "name" := id
            ]
   [ h "span" [] [ t $ "| " <> label <> " |" ]
   ]
