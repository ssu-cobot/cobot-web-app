module Cobot.Controller.Game (
  State, initialState, load, start
  ) where

import Prelude

import Cobot.Game (Game, Layers, initialGame, update)
import Cobot.Game.Parser (layout)
import Cobot.Game.Renderer (renderBackground, renderForeground, tilesize)
import Data.Argonaut.Core as J
import Data.Argonaut.Parser (jsonParser)
import Data.Either (Either(..), note)
import Data.HTTP.Method (Method(GET))
import Data.Int (round)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (log, logShow)
import Effect.Timer (setTimeout)
import Foreign.Object (lookup)
import Graphics.Canvas (CanvasElement, CanvasImageSource, Dimensions, setCanvasWidth, setCanvasHeight, getCanvasElementById, getContext2D, tryLoadImage, scale)
import Partial.Unsafe (unsafePartial)
import Signal (Signal, (<~), (~), (~>), foldp, runSignal, sampleOn)
import Signal.Channel (Channel, channel, send, subscribe)
import Signal.DOM (animationFrame, keyPressed)
import Text.Parsing.Parser (runParser)
import Web.Event.EventTarget (addEventListener, eventListener) as EV
import Web.XHR.EventTypes (load) as XHR
import Web.XHR.ResponseType (ResponseType, string) as XHR
import Web.XHR.XMLHttpRequest (open, response, send, toEventTarget, xmlHttpRequest) as XHR


type State = Maybe Game

initialState :: State
initialState = Nothing


load :: (State -> Effect Unit) -> Effect Unit
load cb = do
  sheet <- loadTilesheet
  game  <- loadGame
  runSignal $ handler <~ game ~ sheet
  where
    handler :: Maybe GetStringResult -> Maybe GetImageResult -> Effect Unit
    handler (Just (Left err)) _ = do
      log err
      cb Nothing
    handler _ (Just (Left err)) = do
      log err
      cb Nothing
    handler (Just (Right gameBody)) (Just (Right tilesheet)) =
      case jsonParser gameBody of
        Left err -> do
          log err
          cb Nothing
        Right game -> case J.toObject game >>= lookup "layout" >>= J.toString of
          Nothing -> cb Nothing
          Just b -> case runParser b layout of
            Left err -> do
              logShow err
              cb Nothing
            Right board -> do
              layers <- getLayers
              dpr <- getDevicePixelRatio
              log $ "dpr: " <> show dpr
              cb $ Just $ initialGame board tilesheet layers dpr
    handler _ _ = pure unit


loadTilesheet :: Effect (Signal (Maybe GetImageResult))
loadTilesheet = getImage "/images/tilesheet.png"

loadGame :: Effect (Signal (Maybe GetStringResult))
loadGame = do
  -- The controller fires before the view updates, but we need to be able to get
  -- the canvases rendered by the view in order to obtain their dimensions and
  -- request the graphics context.  As a workaround, we schedule a callback to
  -- check for the canvas element every 10ms and when it finds it, the request
  -- for the map from the game server can proceed.
  chan <- channel Nothing
  schedule chan
  pure $ subscribe chan
  where
    loadTimeout :: Int
    loadTimeout = 10

    schedule :: Channel (Maybe GetStringResult) -> Effect Unit
    schedule chan = do
      _ <- setTimeout loadTimeout (tryLoad chan)
      pure unit

    tryLoad :: Channel (Maybe GetStringResult) -> Effect Unit
    tryLoad chan = do
      size <- getBackgroundSize
      case size of
        Nothing -> schedule chan
        Just { rows, cols } -> do
          let url = "/game-server?cols=" <> show cols <> "&rows=" <> show rows
          res <- getString url
          runSignal $ res ~> send chan


type Size = { rows :: Int, cols :: Int }

getBackgroundSize :: Effect (Maybe Size)
getBackgroundSize = do
  res <- getCanvasElementById "background-layer"
  case res of
    Nothing -> pure Nothing
    Just canvas -> do
      dims <- getBoundingClientRect canvas
      pure $ Just { rows: round dims.height / tilesize
                  , cols: round dims.width / tilesize
                  }

getLayers :: Effect Layers
getLayers = unsafePartial do
  Just uiCanvas   <- getCanvasElementById "ui-layer"
  Just gameCanvas <- getCanvasElementById "game-layer"
  Just bgCanvas   <- getCanvasElementById "background-layer"
  dpr <- getDevicePixelRatio
  let setupLayer' = setupLayer dpr
  ui   <- setupLayer' uiCanvas
  game <- setupLayer' gameCanvas
  bg   <- setupLayer' bgCanvas
  pure { ui, game, bg }
  where
    setupLayer dpr canvas = do
      dims <- getBoundingClientRect canvas
      setCanvasWidth canvas $ dims.width * dpr
      setCanvasHeight canvas $ dims.height * dpr
      ctx <- getContext2D canvas
      scale ctx { scaleX: dpr, scaleY: dpr }
      pure ctx


type GetResult a = Either String a

get :: ∀ a. String -> XHR.ResponseType a -> Effect (Signal (Maybe (GetResult a)))
get url t = do
  chan <- channel Nothing
  xhr <- XHR.xmlHttpRequest t
  let listener _ev = do
        res <- XHR.response xhr
        send chan $ Just (note "get request failed" res)
  callback <- EV.eventListener listener
  EV.addEventListener XHR.load callback false $ XHR.toEventTarget xhr
  XHR.open (Left GET) url xhr
  XHR.send xhr
  pure $ subscribe chan


type GetStringResult = GetResult String

getString :: String -> Effect (Signal (Maybe GetStringResult))
getString = flip get XHR.string


type GetImageResult = GetResult CanvasImageSource

getImage :: String -> Effect (Signal (Maybe GetImageResult))
getImage url = do
  chan <- channel Nothing
  let callback res = send chan $ Just (note "image load failed" res)
  tryLoadImage url callback
  pure $ subscribe chan


start :: Game -> Effect Unit
start game = do
  log "starting the game"
  -- Render the background layer once.
  renderBackground game
  -- Render the foreground layers every frame.
  frame <- animationFrame
  left  <- keyPressed 0x25
  up    <- keyPressed 0x26
  right <- keyPressed 0x27
  down  <- keyPressed 0x28
  let input = { left: _, up: _, right: _, down: _ }
            <$> left <*> up <*> right <*> down
  let state = foldp update game $ sampleOn frame input
  runSignal (renderForeground <$> state)


foreign import getDevicePixelRatio :: Effect Number
foreign import getBoundingClientRect :: CanvasElement -> Effect Dimensions
