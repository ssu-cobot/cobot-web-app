"use strict";

exports.getDevicePixelRatio = function() {
    return window.devicePixelRatio || 1
}

exports.getBoundingClientRect = function(canvas) {
    return function() {
        return canvas.getBoundingClientRect()
    }
}
