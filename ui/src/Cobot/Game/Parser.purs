module Cobot.Game.Parser (
  Layout, layout, tile, road, ground
  ) where

import Prelude

import Control.Alt ((<|>))
import Data.Array (fromFoldable)
import Data.List (many)
import Text.Parsing.Parser (Parser)
import Text.Parsing.Parser.Combinators (sepEndBy)
import Text.Parsing.Parser.String (string)

import Cobot.Game (GameBoard, Tile(..), GroundStyle(..), RoadStyle(..))


type Layout = GameBoard

layout :: Parser String Layout
layout = fromFoldable <$> layout'
  where
    layout' = row `sepEndBy` (string "\\n")
    row = fromFoldable <$> row'
    row' = many tile

tile :: Parser String Tile
tile = road <|> ground

road :: Parser String Tile
road = (string "|" >>= \_ -> pure $ Road Unmarked 0)
   <|> (string "-" >>= \_ -> pure $ Road Unmarked 0)
   <|> (string "O" >>= \_ -> pure $ Road Unmarked 0)

ground :: Parser String Tile
ground = (string "x" >>= \_ -> pure $ Ground Grass 0)
