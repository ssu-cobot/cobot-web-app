"use strict";

exports.getContextCanvas = function(ctx) {
    return function() {
        return ctx.canvas
    }
}
