module Cobot.Game.Renderer (
  renderBackground,
  renderForeground,
  tilesize
  ) where

import Prelude
import Cobot.Game
import Data.Maybe (Maybe(..))
import Data.FoldableWithIndex (traverseWithIndex_)
import Data.Int (toNumber)
import Effect (Effect)
import Foreign.Object (lookup)
import Graphics.Canvas

tilesize :: Int
tilesize = 16

gap :: Int
gap = 1

tileshift :: Int
tileshift = tilesize + gap

sheetwidth :: Int
sheetwidth = 37


-- | The position of a tile with index `i` in a given grid.
-- |
-- | A grid is specified by its position (`u`,`v`) and width `w`
-- | and is assumed to have unbounded height.
-- |
-- | Example: let u = 1, v = 2, w = 3 (in a 6w x 5h tilesheet)
-- |
-- |      (0,0)   v--(u-1 + w)
-- |        o . . . . .
-- |        . . . . . .
-- |  (u,v) . 0 1 2 . .
-- |        . 3 4 5 . .
-- |        . 6 7 8 . .
-- |
-- |                i
-- | tilemap' 1 2 3 0 = { x: 1, y: 2 }
-- | tilemap' 1 2 3 2 = { x: 3, y: 2 }
-- | tilemap' 1 2 3 6 = { x: 1, y: 4 }
-- | tilemap' 1 2 3 8 = { x: 3, y: 4 }
-- |
-- | This function is useful for selecting a rectangular subregion of the
-- | tilesheet and getting the position of a particular tile in that region by
-- | using a single index.
-- | See the `car`, `road`, `ground`, and `path` functions for further examples.
-- |
-- | NB: This wrapping pattern is extremely common in all sorts of programming!
tilemap' :: Int -> Int -> Int -> Int -> Point
tilemap' u v w i =
  { x: tileshift * ((i `mod` w) + u)
  , y: tileshift * ((i `div` w) + v)
  }

car :: Direction -> CarStyle -> Piece -> Point
car West  = car' 0 0 3 -- 3w x 2h
car East  = car' 3 0 3 -- 3w x 2h
car North = car' 0 2 2 -- 2w x 2h
car South = car' 2 2 2 -- 2w x 2h

car' :: Int -> Int -> Int -> CarStyle -> Piece -> Point
car' u v w s = tilemap' (u + 31) (v + voffs + 16) w
  where
    voffs = case s of
      GreenCar -> 0
      GreyCar -> 4
      RedCar -> 8

road :: RoadStyle -> Piece -> Point
road WhiteStripe  = tilemap' 9 19 2 -- 2w x 6h
road YellowStripe = yellowstripe
  where
    yellowstripe p = tilemap' 11 19 4 (p + 2) -- 4w x 3h minus first two pieces
road EdgeStripe   = tilemap' 15 21 4 -- 4w x 4h
road Crosswalk    = tilemap' 9 22 6 -- 6w x 1h
road ArrowMark    = tilemap' 19 21 1 -- 1w x 4h
road SymbolMark   = symbolmark
  where
    symbolmark 13 = tilemap' 12 19 1 0 -- 1w x 1h
    symbolmark p  = tilemap' 9 23 6 p -- 6w x 2h
road Unmarked     = unmarked
  where
    unmarked 0 = tilemap' 11 19 1 0 -- 1w x 1h
    unmarked _ = tilemap' 10 24 1 0 -- 1w x 1h

ground :: GroundStyle -> Piece -> Point
ground Grass = tilemap' 0 24 2 -- 2w x 1h
ground Stone = tilemap' 2 24 2 -- 2w x 1h
ground Dirt  = tilemap' 4 24 2 -- 2w x 1h
ground Sand  = tilemap' 6 24 2 -- 2w x 1h

path :: PathStyle -> Piece -> Point
path s = tilemap' x 19 3
  where
    x = case s of
      DarkGreyBrick -> 0
      SandBrick -> 3
      LightGreyBrick -> 6

tilemap :: Tile -> Point
tilemap (Car d s p) = car d s p
tilemap (Road s p) = road s p
tilemap (Ground s p) = ground s p
tilemap (Path s p) = path s p


drawTile :: Context2D -> Tilesheet -> Tile -> Point -> Effect Unit
drawTile ctx sheet tile pos = drawImageFull ctx sheet sx sy ts ts dx dy ts ts
  where
    s = tilemap tile
    sx = toNumber s.x
    sy = toNumber s.y
    dx = toNumber pos.x
    dy = toNumber pos.y
    ts = toNumber tilesize


drawCar :: Context2D -> Tilesheet -> Direction -> CarStyle -> Position -> Effect Unit
drawCar ctx sheet dir style pos = withContext ctx $ do
  translate ctx { translateX: pos.x * ts, translateY: pos.y * ts }
  case dir of
    North -> drawVertical
    South -> drawVertical
    East  -> drawHorizontal
    West  -> drawHorizontal
  where
    ts = toNumber tilesize
    drawVertical = do
      drawTile ctx sheet (Car dir style 0) { x: 0, y: 0 }
      drawTile ctx sheet (Car dir style 1) { x: tilesize, y: 0 }
      drawTile ctx sheet (Car dir style 2) { x: 0, y: tilesize }
      drawTile ctx sheet (Car dir style 3) { x: tilesize, y: tilesize }
    drawHorizontal = do
      drawTile ctx sheet (Car dir style 0) { x: 0, y: 0 }
      drawTile ctx sheet (Car dir style 1) { x: tilesize, y: 0 }
      drawTile ctx sheet (Car dir style 2) { x: 2 * tilesize, y: 0 }
      drawTile ctx sheet (Car dir style 3) { x: 0, y: tilesize }
      drawTile ctx sheet (Car dir style 4) { x: tilesize, y: tilesize }
      drawTile ctx sheet (Car dir style 5) { x: 2 * tilesize, y: tilesize }


renderBackground :: Game -> Effect Unit
renderBackground { board, tilesheet, layers } =
  renderBackground' layers.bg tilesheet board

renderBackground' :: Layer -> Tilesheet -> GameBoard -> Effect Unit
renderBackground' ctx sheet = traverseWithIndex_ renderGameBoardRow
  where renderGameBoardRow row = traverseWithIndex_ renderGameBoardTile
          where renderGameBoardTile col tile = drawTile ctx sheet tile location
                  where location = { x: tilesize * col, y: tilesize * row }


renderForeground :: Game -> Effect Unit
renderForeground { board, tilesheet, layers, player } = do
  gameCanvas <- getContextCanvas layers.game
  dims <- getCanvasDimensions gameCanvas
  clearRect layers.game { x: 0.0, y: 0.0, width: dims.width, height: dims.height }
  drawCar layers.game tilesheet player.direction RedCar player.position


foreign import getContextCanvas :: Context2D -> Effect CanvasElement
