User Interface
==============

This project builds the static assets for the user interface of [cobot].


Prerequisites
-------------

This project use PureScript, Pulp, and Bower, which can be installed with npm:
```
npm install -g purescript pulp bower
```


Dependencies
------------

The PureScript dependencies are fetched using Bower:
```
bower install
```


Development
-----------

To do a debug build:
```
pulp browserify --to dist/scripts/cobot.js
```

To start a local web server for development:
```
pulp server
```
Then visit http://localhost:1337/dist in your web browser.

The development server watches the source directory for changes and rebuilds the
application automatically.


Production
----------

To do an optimized build:
```
pulp browserify --optimise --to dist/scripts/cobot.js
```

The dist/ directory contains the static content to be placed on the web server.
