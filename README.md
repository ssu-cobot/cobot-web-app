[cobot] web app
===============

This is the monorepo for the [cobot] web application.

Deployment
----------

The supported deployment method uses [uwsgi][] + [nginx][].

[uwsgi]: https://uwsgi-docs.readthedocs.io/en/latest/index.html
[nginx]: https://nginx.org/en/

Once these are installed, the config files in etc/ can be used to run the
service as follows (these instructions may need adjustment for your OS):

```sh
#
# Initial setup (performed once)
#

# Get a copy of the repository
git clone https://gitlab.com/ssu-cobot/cobot-web-app
cd cobot-web-app

# Create files for uwsgi
touch /var/log/cobot-game-server.log /tmp/cobot-game-server.sock

# Back up the existing nginx.conf and install the sample config, e.g.:
ln -sf $(pwd)/etc/nginx/nginx.conf ${nginx_config_path:?fail}/nginx.conf


#
# Start the services
#

nginx
uwsgi --master --workers 4 \
      --yaml etc/uwsgi/game-server.yaml \
      --daemonize2 /var/log/cobot-game-server.log
```
